object frmPrincipal: TfrmPrincipal
  Left = 0
  Top = 0
  Caption = 'UC Script Generator (Multiple Users)'
  ClientHeight = 506
  ClientWidth = 955
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    955
    506)
  PixelsPerInch = 120
  TextHeight = 17
  object lblUCLogin: TLabel
    Left = 21
    Top = 31
    Width = 60
    Height = 17
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'UC Login:'
  end
  object lblAuthors: TLabel
    Left = 10
    Top = 466
    Width = 432
    Height = 17
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Anchors = [akLeft, akBottom]
    Caption = 
      'Program developed by Victor Westmann and idealized by Rafael Mar' +
      'ta.'
  end
  object edtNome: TEdit
    Left = 126
    Top = 27
    Width = 613
    Height = 25
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnKeyPress = edtNomeKeyPress
  end
  object btnScript: TButton
    Left = 756
    Top = 25
    Width = 169
    Height = 33
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Anchors = [akTop, akRight]
    Caption = 'Generate &SCRIPT'
    TabOrder = 1
    OnClick = btnScriptClick
  end
  object memoOutput: TMemo
    Left = 21
    Top = 65
    Width = 904
    Height = 365
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'Script output here...')
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object btnClipboard: TButton
    Left = 756
    Top = 459
    Width = 169
    Height = 33
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Anchors = [akRight, akBottom]
    Caption = 'Copy to &Clipboard'
    TabOrder = 3
    OnClick = btnClipboardClick
  end
end
