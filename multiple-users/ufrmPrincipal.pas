unit ufrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TfrmPrincipal = class(TForm)
    lblUCLogin: TLabel;
    edtNome: TEdit;
    btnScript: TButton;
    memoOutput: TMemo;
    btnClipboard: TButton;
    lblAuthors: TLabel;
    procedure btnClipboardClick(Sender: TObject);
    procedure btnScriptClick(Sender: TObject);
    procedure edtNomeKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

procedure TfrmPrincipal.btnClipboardClick(Sender: TObject);
begin
  memoOutput.SelectAll;
  memoOutput.CopyToClipboard;
end;

{http://stackoverflow.com/questions/2625707/split-a-string-into-an-array-of-strings-based-on-a-delimiter}
procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter     := Delimiter;
   ListOfStrings.DelimitedText := Str;
end;



{/*
// Implement features requested above
*/}

//DONE -cDONE -oVictor Westmann: implement multiple users input
//DONE -cDONE -oVictor: Apertar o enter no edit e disparar o bot�o



procedure TfrmPrincipal.btnScriptClick(Sender: TObject);
var
  i       : Integer; //�ndice
  Texto   : String;  //para manipular as partes da string inicial
  Dado    : String;
  header  : String;  // mostrar� a parte fixa do script. d�! ;)
  script  : String;  // ter� o texto que muda com os nomes
begin
  if edtNome.Text = '' then
    begin
      memoOutput.Text := 'Please enter a user ID!';
    end
  else
  begin
    i:=1;
    texto:='';
    Dado  := edtNome.Text;
    memoOutput.Text := ''; // inicializando o memo
    memoOutput.Lines.Add('##ExternalId;externalId;assignedUserIdentity;assignedExternalSystemId;isContactMaster;isProvisioningMaster;additionalInfo');
  While Copy(Dado,i,1) <> ''  do
  begin
    While (Copy(Dado,i,1) <> ',') and (Copy(Dado,i,1)<>'') do
    begin
      texto:=texto+Copy(dado,i,1);
      i:=i+1;
    end;
    header :=
    '##ExternalId;externalId;assignedUserIdentity;assignedExternalSystemId;isContactMaster;isProvisioningMaster;additionalInfo'+ sLineBreak;
    script :=
    'ExternalId; '+Texto+'@PA.LCL; '+Texto+'@system;WinPassphrase;false;false;'+ sLineBreak +
    'ExternalId; '+Texto+';'+Texto+'@system;PA.LCL;false;false;';
    memoOutput.Lines.Add(script);
    Texto:='';
    i:=i+1;
  end
  end
end;

procedure TfrmPrincipal.edtNomeKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    btnScriptClick(Sender);
end;

function GetCurrentDateTime: TDateTime;
var
  SystemTime: TSystemTime;
begin
  GetLocalTime(SystemTime);
  Result := SystemTimeToDateTime(SystemTime);
end;

end.
