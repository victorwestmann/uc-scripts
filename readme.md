# Welcome
Generate scripts to create users in **UC Platform** easier.

---

To use it, simply choose your version:

* Single User
* Multiple Users _(from 2 to n)_

### When was this created?
October/2013.

### Why was this created?
To make the user creation in **UC OpenScape Voice** (product from Unify (ex Siemens Enterprise)) easier/less boring/painful. ;-)

### In which programming language was this created?
This was created in Object Pascal language. We used **Embarcadero Delphi XE4** _(with a fancy skin)_.

### What is this program license?
GPL-v2. (http://opensource.org/licenses/GPL-2.0)

### What is the author?
**Victor Westmann**.

---
UC = Unified Communications.

To learn the basics of using Markdown, **[read this](http://daringfireball.net/projects/markdown/basics)**.
