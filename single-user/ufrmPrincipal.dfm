object frmPrincipal: TfrmPrincipal
  Left = 0
  Top = 0
  Caption = 'UC Script Generator'
  ClientHeight = 462
  ClientWidth = 894
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    894
    462)
  PixelsPerInch = 120
  TextHeight = 17
  object lblUCLogin: TLabel
    Left = 21
    Top = 73
    Width = 60
    Height = 17
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'UC Login:'
  end
  object lblAuthors: TLabel
    Left = 21
    Top = 421
    Width = 432
    Height = 17
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Anchors = [akLeft, akBottom]
    Caption = 
      'Program developed by Victor Westmann and idealized by Rafael Mar' +
      'ta.'
  end
  object lblWindowsLogin: TLabel
    Left = 21
    Top = 33
    Width = 98
    Height = 17
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Windows Login:'
  end
  object edtUCLogin: TEdit
    Left = 136
    Top = 27
    Width = 543
    Height = 25
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
  end
  object btnScript: TButton
    Left = 696
    Top = 27
    Width = 168
    Height = 70
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Anchors = [akTop, akRight]
    Caption = 'Generate &SCRIPT'
    TabOrder = 2
    OnClick = btnScriptClick
  end
  object memoOutput: TMemo
    Left = 21
    Top = 123
    Width = 843
    Height = 265
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'Script output here...')
    ScrollBars = ssBoth
    TabOrder = 4
  end
  object btnClipboard: TButton
    Left = 696
    Top = 415
    Width = 168
    Height = 32
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Anchors = [akRight, akBottom]
    Caption = 'Copy to &Clipboard'
    TabOrder = 3
    OnClick = btnClipboardClick
  end
  object edtWLogin: TEdit
    Left = 136
    Top = 69
    Width = 543
    Height = 25
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    OnKeyPress = edtWLoginKeyPress
  end
end
