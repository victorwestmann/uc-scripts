unit ufrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TfrmPrincipal = class(TForm)
    lblUCLogin: TLabel;
    edtUCLogin: TEdit;
    btnScript: TButton;
    memoOutput: TMemo;
    btnClipboard: TButton;
    lblAuthors: TLabel;
    lblWindowsLogin: TLabel;
    edtWLogin: TEdit;
    procedure btnClipboardClick(Sender: TObject);
    procedure btnScriptClick(Sender: TObject);
    procedure edtWLoginKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

procedure TfrmPrincipal.btnClipboardClick(Sender: TObject);
begin
  memoOutput.SelectAll;
  memoOutput.CopyToClipboard;
end;

{/*
// Implement features requested above
*/}

//TODO -cFeature Rrquest -oVictor Westmann: implement multiple users input
//TODO -cFeature Request -oVictor: Apertar o enter no edit e disparar o bot�o



procedure TfrmPrincipal.btnScriptClick(Sender: TObject);
var
  output : String;
begin
  if (edtUCLogin.Text = '') OR (edtWLogin.Text = '') then
  begin
    memoOutput.Text := 'Please fill in both UC and Windows Login!';
  end
  else
  begin
    output := '##ExternalId;externalId;assignedUserIdentity;assignedExternalSystemId;isContactMaster;isProvisioningMaster;additionalInfo' + sLineBreak +
              'ExternalId;'+edtUCLogin.Text+'@GLOBAL.IFF.COM;'+edtWLogin.Text+'@system;WinPassphrase;false;false;' + sLineBreak +
              'ExternalId;'+edtUCLogin.Text+'@GLOBAL.IFF.COM;'+edtWLogin.Text+'@system;Windows User Account;false;false;';
    memoOutput.Text := output;
  end
end;


procedure TfrmPrincipal.edtWLoginKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    btnScriptClick(Sender);
end;

end.
